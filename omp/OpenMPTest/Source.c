#include <stdio.h>
#include <omp.h>
#define N 10

int main()
{
	omp_set_num_threads(2);
	/*omp_set_num_threads(16);
	#pragma omp parallel num_threads(4)
	{
		printf("Hello world\n");
	}

	printf("%d\n", omp_get_num_procs());

	//omp_get_thread_num();

	#pragma omp master
	{
		printf("%d\n", omp_get_thread_num());
	}

	#pragma omp single
	{

	}
	*/


	int i = 0;
	int j = 0;

#pragma omp parallel private(j) shared()
	{
#pragma omp for
		for (i = 0; i < N; i++)
		{
			for (j = 0; j < N; j++)
			{
				printf("i-%d j-%d z vlakna %d\n", i, j, omp_get_thread_num());
			}
		}
	}

	getchar();

	return 0;
}