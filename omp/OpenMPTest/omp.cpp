#include <stdio.h>
#include <omp.h>
#define N 100
 
int A[N * N];
 
int main() {
    omp_set_num_threads(12);
 
    /*#pragma omp parallel //num_threads(3)
        {
            if (omp_get_thread_num() == 0) {
                printf("vypisuje Hello World z vlakna %d\n", omp_get_thread_num());
            }*/
 
            /*#pragma omp single
                    {
                        printf("vypisuje Hello World z vlakna %d\n", omp_get_thread_num());
                    }
}*/
 
//printf("%d\n", omp_get_num_procs());
 
/*int i = 10;
 
#pragma omp parallel
    {
        int j = 20;
        printf("i=%d z vlakna %d\n", i, omp_get_thread_num());
        printf("j=%d z vlakna %d\n", j, omp_get_thread_num());
    }*/
 
    /*int i = 0;
 
#pragma omp parallel
    {
#pragma omp for
        for (i = 0; i < N; i++) {
            printf("i=%d z vlakna %d\n", i, omp_get_thread_num());
        }
    }*/
 
    /*int i = 0;
    int j = 0; // share
#pragma omp parallel private(j)
    {
#pragma omp for
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++)
            {
                printf("i=%d j=%d z vlakna %d\n", i, j, omp_get_thread_num());
            }
        }
    }*/
 
    /*int i = 0;
    int j = 0; // share
    int pom = 1;
#pragma omp parallel for firstprivate(pom) lastprivate(pom)
    for (i = 0; i < N; i++) {
        pom = (i + 1) * pom;
        printf("i=%d pom=%d z vlakna %d\n", i, pom, omp_get_thread_num());
    }
 
    printf(" pom=%d\n", pom);*/
 
    /*int i;
    int pom = 0;
#pragma omp parallel for
    for (i = 0; i < N * 10000; i++) {
#pragma omp atomic
        pom++;
    }
 
    printf(" pom=%d\n", pom);*/
    /*  int i;
 
    #pragma omp parallel for ordered schedule(dynamic)
        for (i = 0; i < N; i++) {
    #pragma omp ordered
            printf("i=%d z vlakna %d\n", i, omp_get_thread_num());
        }*/
 
        /*int i, j;
        long max = 0;
    #pragma omp parallel for private(j)
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++)
            {
                A[(i * N) + j] = i * j;
    #pragma omp critical
                {
                    if (A[(i * N) + j] > max) {
                        max = A[(i * N) + j];
                    }
                }
            }
        }
 
        printf("max = %d", max);*/
 
        /*int a = 0, i;
    #pragma omp parallel reduction(+:a)
        {
            a = 1;
        }
        printf("%d\n", a);*/
 
        /*int fac = 1;
        int i;
    #pragma omp parallel for reduction(*:fac)
        for (i = 2; i <= N; i++)
        {
            fac *= i;
        }
 
        printf_s("%d\n", fac);*/
 
    int i, j;
    long long sum = 0;
#pragma omp parallel for private(j) //reduction(+:sum)
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++)
        {
            A[(i * N) + j] = i * j;
            sum += A[(i * N) + j];
        }
        //printf("Suma na vlakne %d = %lld\n", omp_get_thread_num(), sum);
    }
    printf("Suma  = %lld\n", sum);
 
    getchar();
}