#include <iostream>
#include <omp.h>
#include <stdio.h>
#include <fstream>
#include <cmath>
#include <string>
#include <sstream>
#include <vector>
#include <sstream>
#include <chrono>
#include <immintrin.h>
#include <stdlib.h>
/********************************/

#define M_PI 3.14159265359
inline constexpr double deg2rad(double x) { return x * M_PI / 180.; }
inline constexpr double rad2deg(double x) { return x * 180. / M_PI; }

constexpr int ALIGNMENT = 32;
constexpr const char* fileName("BL-32402.dat");
constexpr int DATA_SIZE = 32402;
constexpr int EARTH_RADIUS_KM = 6378000;	/*6378*/
constexpr double GM = 398600.5;
constexpr int D = 50000;
constexpr int MAX_ITER = 100;
constexpr double TOL = 1e-6;

struct Point3D
{
	Point3D() {};
	Point3D(double xx, double yy, double zz) : x(xx), y(yy), z(zz) {};
	inline Point3D operator-(const Point3D& point) const { return Point3D(x - point.x, y - point.y, z - point.z); }
	inline Point3D operator+(const Point3D& point) const { return Point3D(x + point.x, y + point.y, z + point.z); }
	inline Point3D operator*(double val) const { return Point3D(x * val, y * val, z * val); }
	inline Point3D operator/(double val) const { return Point3D(x / val, y / val, z / val); }
	inline double Norm() const { return sqrt(x * x + y * y + z * z); }
	Point3D Normalized() const;
	inline double Dot(const Point3D& point) const { return x * point.x + y * point.y + z * point.z; }

	double x;
	double y;
	double z;
};

Point3D Point3D::Normalized() const
{
	double norm = Norm();
	return Point3D(x / norm, y / norm, z / norm);
}

class DVector
{
public:
	DVector();
	DVector(size_t length);

	DVector(const DVector &vec);
	DVector(DVector &&vec);
	DVector &operator=(const DVector &vec);
	DVector &operator=(DVector &&vec);
	~DVector();

	void SetLength(size_t length);
	inline size_t GetLength() const { return m_length; }
	inline double *GetValues() { return m_values; }
	void SetValues(double val);
	inline const double* GetConstValues() const { return m_values; }
	void AddElement(double val);

	DVector operator*(double val) const;
	DVector operator/(double val) const;
	DVector operator-(const DVector &vec) const;
	DVector operator+(const DVector &vec) const;
	inline double& operator[](size_t i) { return m_values[i]; }
	inline double operator[](size_t i) const { return m_values[i]; }

	void Subtract(const DVector& vec, DVector& out) const;
	void Add(const DVector& vec, DVector& out) const;
	void Divide(double val, DVector& out) const;
	void Multiply(double val, DVector& out) const;
	void CopyFrom(const DVector& from);
	void SwapWith(DVector& from);

	double Dot(const DVector& vec) const;
	double Norm() const;
	void Normalize();
	DVector Normalized() const;

private:
	double *m_values;
	size_t m_length;

	inline void AllocateDataArray(size_t length) { m_values = static_cast<double*>(_aligned_malloc(length * sizeof(double), ALIGNMENT)); }
	void DeallocateDataArray();
};

void DVector::SwapWith(DVector& from)
{
	double* buff = m_values;

	m_values = from.m_values;
	from.m_values = buff;
}

void DVector::CopyFrom(const DVector& from)
{
	memcpy(static_cast<void*>(m_values), static_cast<void*>(from.m_values), DATA_SIZE * sizeof(double));
}

void DVector::Subtract(const DVector& vec, DVector& out) const
{
	size_t i = 0;

	if (DATA_SIZE >= 4)
	{
		for (i; i <= DATA_SIZE - 4; i += 4)
		{
			_mm256_store_pd(out.m_values + i, _mm256_sub_pd(_mm256_load_pd(m_values + i), _mm256_load_pd(vec.m_values + i)));
		}
	}

	if (DATA_SIZE % 4 != 0)
	{
		for (i; i < DATA_SIZE; i++)
		{
			out[i] = m_values[i] - vec.m_values[i];
		}
	}
}

void DVector::Add(const DVector& vec, DVector& out) const
{
	size_t i = 0;

	if (DATA_SIZE >= 4)
	{
		for (i; i <= DATA_SIZE - 4; i += 4)
		{
			_mm256_store_pd(out.m_values + i, _mm256_add_pd(_mm256_load_pd(m_values + i), _mm256_load_pd(vec.m_values + i)));
		}
	}

	if (DATA_SIZE % 4 != 0)
	{
		for (i; i < DATA_SIZE; i++)
		{
			out[i] = m_values[i] + vec.m_values[i];
		}
	}
}

void DVector::Divide(double val, DVector& out) const
{
	size_t i = 0;
	double arr[4] = { val, val, val, val };
	__m256d divisor(_mm256_load_pd(arr));

	if (DATA_SIZE >= 4)
	{
		for (i; i <= DATA_SIZE - 4; i += 4)
		{
			_mm256_store_pd(out.m_values + i, _mm256_div_pd(_mm256_load_pd(m_values + i), divisor));
		}
	}

	if (DATA_SIZE % 4 != 0)
	{
		for (i; i < DATA_SIZE; i++)
		{
			out[i] = m_values[i] / val;
		}
	}
}

void DVector::Multiply(double val, DVector& out) const
{
	size_t i = 0;
	double arr[4] = { val, val, val, val };
	__m256d multiplicator(_mm256_load_pd(arr));

	if (DATA_SIZE >= 4)
	{
		for (i; i <= DATA_SIZE - 4; i += 4)
		{
			_mm256_store_pd(out.m_values + i, _mm256_mul_pd(_mm256_load_pd(m_values + i), multiplicator));
		}
	}

	if (DATA_SIZE % 4 != 0)
	{
		for (i; i < DATA_SIZE; i++)
		{
			out[i] = m_values[i] * val;
		}
	}
}

DVector::DVector()
{
	m_values = nullptr;
	m_length = 0;
}

DVector::DVector(size_t length)
{
	m_values = nullptr;
	this->m_length = length;

	AllocateDataArray(length);

	for (size_t i = 0; i < length; i++)
	{
		m_values[i] = 0.;
	}
}

DVector::DVector(const DVector &vec)
{
	AllocateDataArray(vec.m_length);
	m_length = vec.m_length;

	for (size_t i = 0; i < m_length; i++)
	{
		m_values[i] = vec.m_values[i];
	}
}

DVector::DVector(DVector &&vec)
{
	m_values = vec.m_values;
	vec.m_values = nullptr;
	m_length = vec.m_length;
}

DVector &DVector::operator=(const DVector &vec)
{
	DeallocateDataArray();

	if (m_values == nullptr) {
		AllocateDataArray(vec.m_length);

		for (size_t i = 0; i < vec.m_length; i++)
		{
			m_values[i] = vec.m_values[i];
		}
		m_length = vec.m_length;
	}

	return *this;
}

DVector &DVector::operator=(DVector &&vec)
{
	DeallocateDataArray();

	if (m_values == nullptr) {
		m_values = vec.m_values;
		vec.m_values = nullptr;
		m_length = vec.m_length;
	}

	return *this;
}

DVector::~DVector()
{
	DeallocateDataArray();
}

void DVector::SetValues(double val)
{
	for (size_t i = 0; i <DATA_SIZE; i++)
	{
		m_values[i] = val;
	}
}

void DVector::DeallocateDataArray()
{
	if (m_values != nullptr) {
		_aligned_free(m_values);
	}

	m_values = nullptr;
}

void DVector::SetLength(size_t length)
{
	DeallocateDataArray();
	AllocateDataArray(length);
	this->m_length = length;
}

DVector DVector::operator*(double val) const
{
	DVector buff(m_length);

	for (size_t i = 0; i < m_length; i++)
	{
		buff.m_values[i] = m_values[i] * val;
	}

	return std::move(buff);
}

DVector DVector::operator/(double val) const
{
	DVector buff(m_length);

	for (size_t i = 0; i < m_length; i++)
	{
		buff.m_values[i] = m_values[i] / val;
	}

	return std::move(buff);
}

DVector DVector::operator-(const DVector &vec) const
{
	DVector buff(m_length);

	for (size_t i = 0; i < m_length; i++)
	{
		buff.m_values[i] = m_values[i] - vec.m_values[i];
	}

	return std::move(buff);
}

DVector DVector::operator+(const DVector &vec) const
{
	DVector buff(m_length);

	for (size_t i = 0; i < m_length; i++)
	{
		buff.m_values[i] = m_values[i] + vec.m_values[i];
	}

	return std::move(buff);
}

double DVector::Norm() const
{
	double buff = 0;

	for (size_t i = 0; i < DATA_SIZE; i++)
	{
		buff += (m_values[i] * m_values[i]);
	}

	return sqrt(buff);
}

void DVector::AddElement(double val)
{
	double *newData = new double[m_length + 1];

	for (size_t i = 0; i < m_length; i++)
	{
		newData[i] = m_values[i];
	}

	newData[m_length] = val;

	DeallocateDataArray();

	m_values = newData;
	m_length++;
}

double DVector::Dot(const DVector& vec) const
{
	double result[4];
	__m256d sum = _mm256_setzero_pd();
	size_t i = 0;

	if (DATA_SIZE >= 4)
	{
		for (i; i <= DATA_SIZE - 4; i += 4)
		{
			sum = _mm256_add_pd(sum, _mm256_mul_pd(_mm256_load_pd(m_values + i), _mm256_load_pd(vec.m_values + i)));
		}
	}

	_mm256_store_pd(result, sum);

	if (DATA_SIZE % 4 != 0)
	{
		for (i; i < DATA_SIZE; i++)
		{
			result[0] += m_values[i] * vec.m_values[i];
		}
	}

	return result[0] + result[1] + result[2] + result[3];

}

void DVector::Normalize()
{
	double norm = Norm();

	for (size_t i = 0; i < m_length; i++)
	{
		m_values[i] /= norm;
	}
}

DVector DVector::Normalized() const
{
	DVector temp(*this);

	temp.Normalize();

	return std::move(temp);
}

class DMatrix
{
public:
	DMatrix(size_t columns, size_t rows);
	DMatrix(const DMatrix &mat);
	DMatrix(DMatrix &&mat);
	DMatrix& operator=(const DMatrix &mat);
	DMatrix& operator=(DMatrix &&mat);
	inline DVector& operator[](size_t i) { return m_rows[i]; }
	inline const DVector& operator[](size_t i) const { return m_rows[i]; }
	~DMatrix();

	void InsertBlock(const DMatrix& mat, int row, int col);
	inline size_t ColumnCount() const { return m_columnCount; }
	inline size_t RowCount() const { return m_rowCount; }
	DMatrix operator*(const DMatrix &mat) const;
	DVector operator*(const DVector &vec) const;
	DMatrix operator*(double val) const;
	inline DVector GetRow(const int index) const { return m_rows[index]; }
	inline DVector *GetRows() { return m_rows; }
	inline const DVector* GetConstRows() const { return m_rows; }
	DMatrix Transpose() const;
	DVector ColumnToVector(int colIndex) const;
	void MakeIdentity();
	void SwapColumns(int index1, int index2);
	void SwapRows(int index1, int index2);
	void SetColumn(int index, const DVector& vec);

private:
	DVector *m_rows;
	size_t m_columnCount;
	size_t m_rowCount;

	void DeallocateDataArray();
	inline void AllocateDataArray(size_t count) { m_rows = new DVector[count]; }
	DMatrix CopyThisObject() const;
};

DMatrix::DMatrix(size_t columns, size_t rows)
{
	m_rows = nullptr;
	AllocateDataArray(rows);

	for (size_t i = 0; i < rows; i++)
	{
		m_rows[i].SetLength(columns);
	}

	this->m_columnCount = columns;
	this->m_rowCount = rows;
}

DMatrix::DMatrix(const DMatrix &mat)
{
	m_rows = nullptr;
	AllocateDataArray(mat.m_rowCount);

	for (int i = 0; i < mat.m_rowCount; i++)
	{
		m_rows[i].SetLength(mat.m_columnCount);
		m_rows[i] = mat.m_rows[i];
	}

	m_columnCount = mat.m_columnCount;
	m_rowCount = mat.m_rowCount;
}

DMatrix::DMatrix(DMatrix &&mat)
{
	m_rows = mat.m_rows;
	mat.m_rows = nullptr;

	m_columnCount = mat.m_columnCount;
	m_rowCount = mat.m_rowCount;
}

DMatrix &DMatrix::operator=(const DMatrix &mat)
{
	DeallocateDataArray();

	if (m_rows == nullptr)
	{
		AllocateDataArray(mat.m_rowCount);

		for (int i = 0; i < mat.m_rowCount; i++)
		{
			m_rows[i].SetLength(mat.m_columnCount);
			m_rows[i] = mat.m_rows[i];
		}
	}

	m_columnCount = mat.m_columnCount;
	m_rowCount = mat.m_rowCount;

	return *this;
}

DMatrix &DMatrix::operator=(DMatrix &&mat)
{
	DeallocateDataArray();

	m_rows = mat.m_rows;
	mat.m_rows = nullptr;

	m_columnCount = mat.m_columnCount;
	m_rowCount = mat.m_rowCount;

	return *this;
}

DMatrix::~DMatrix()
{
	DeallocateDataArray();
}

void DMatrix::InsertBlock(const DMatrix& mat, int row, int col)
{
	for (int i = row; i < row + mat.m_rowCount; i++)
	{
		for (int j = col; j < col + mat.m_columnCount; j++)
		{
			m_rows[i].GetValues()[j] = mat.m_rows[i - row].GetValues()[j - col];
		}
	}
}

void DMatrix::DeallocateDataArray()
{
	if (m_rows != nullptr) {
		delete[] m_rows;
	}

	m_rows = nullptr;
}

DMatrix DMatrix::operator*(const DMatrix &mat) const
{
	DMatrix mBuff(mat.m_columnCount, mat.m_rowCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		for (int j = 0; j < mat.m_columnCount; j++)
		{
			DVector matCol(mat.ColumnToVector(j));
			mBuff.m_rows[i].GetValues()[j] = m_rows[i].Dot(matCol);
		}
	}

	return std::move(mBuff);
}

DVector DMatrix::operator*(const DVector &vec) const
{
	DVector vBuff(m_rowCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		vBuff.GetValues()[i] = m_rows[i].Dot(vec);
	}

	return std::move(vBuff);
}

DMatrix DMatrix::operator*(double val) const
{
	DMatrix buff(m_columnCount, m_rowCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		buff.m_rows[i] = m_rows[i] * val;
	}

	return std::move(buff);
}

DVector DMatrix::ColumnToVector(int colIndex) const
{
	DVector buff(m_rowCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		buff.GetValues()[i] = m_rows[i].GetValues()[colIndex];
	}

	return buff;
}

DMatrix DMatrix::Transpose() const
{
	DMatrix mBuff(m_rowCount, m_columnCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		mBuff.m_rows[i] = ColumnToVector(i);
	}

	return mBuff;
}

void DMatrix::MakeIdentity() {
	for (int i = 0; i < m_rowCount; i++)
	{
		for (int j = 0; j < m_columnCount; j++)
		{
			if (i == j)
			{
				m_rows[i].GetValues()[j] = 1.;
			}
			else
			{
				m_rows[i].GetValues()[j] = 0.;
			}
		}
	}
}

void DMatrix::SwapRows(int index1, int index2)
{
	DVector buff;

	buff = m_rows[index1];
	m_rows[index1] = m_rows[index2];
	m_rows[index2] = buff;
}

void DMatrix::SwapColumns(int index1, int index2)
{
	DVector buff1, buff2;

	buff1 = ColumnToVector(index1);
	buff2 = ColumnToVector(index2);

	SetColumn(index1, buff1);
	SetColumn(index2, buff2);
}

void DMatrix::SetColumn(int index, const DVector& vec)
{
	for (int i = 0; i < m_rowCount; i++)
	{
		m_rows[i].GetValues()[index] = vec.GetConstValues()[i];
	}
}

DMatrix DMatrix::CopyThisObject() const
{
	DMatrix newMat(m_columnCount, m_rowCount);

	for (int i = 0; i < m_columnCount; i++)
	{
		for (int j = 0; j < m_rowCount; j++)
		{
			newMat.GetRows()[i].GetValues()[j] = m_rows[i].GetValues()[j];
		}
	}

	return newMat;
}

struct GeoData
{
	GeoData(double BB, double LL, double HH, double DG, double TZZ) : B(BB), L(LL), H(HH), dg(DG), Tzz(TZZ) {};
	double B;
	double L;
	double H;
	double dg;
	double Tzz;
};

void LoadGeoData(const std::string& pathToFile, std::vector<GeoData>& dataVec)
{
	std::ifstream inFile(pathToFile);

	if (!inFile.is_open())
	{
		std::cerr << "Could not open file!";
	}

	double first, second, third, fourth, fifth;

	while (inFile >> first >> second >> third >> fourth >> fifth)
	{
		dataVec.emplace_back(deg2rad(first), deg2rad(second), third, fourth, fifth);
	}

	inFile.close();
}

void ConvertToXCartesian(const std::vector<GeoData>& from, std::vector<Point3D>& to)
{
#pragma omp parallel for
	for (int i = 0; i < DATA_SIZE; i++)
	{
		const double cosB = cos(from[i].B);
		const double radius = EARTH_RADIUS_KM + from[i].H;

		to[i] = Point3D(radius * cosB * cos(from[i].L),
			radius * cosB * sin(from[i].L),
			radius * sin(from[i].B)
		);
	}
}

void ConvertToSCartesian(const std::vector<GeoData>& from, std::vector<Point3D>& to)
{
#pragma omp parallel for
	for (int i = 0; i < DATA_SIZE; i++)
	{
		const double cosB = cos(from[i].B);
		const double radius = EARTH_RADIUS_KM + from[i].H - D;

		to[i] = Point3D(radius * cosB * cos(from[i].L),
			radius * cosB * sin(from[i].L),
			radius * sin(from[i].B)
		);
	}
}

void FillMatrixParallel(const std::vector<Point3D>& x, const std::vector<Point3D>& s, DMatrix& A)
{
#pragma omp parallel for
	for (int i = 0; i < DATA_SIZE; i++)
	{
		Point3D e(x[i].Normalized());
		double* matRow = A.GetRows()[i].GetValues();

		for (size_t j = 0; j < DATA_SIZE; j++)
		{
			Point3D d(x[i] - s[j]);
			double dNorm(d.Norm());

			matRow[j] = (d.Dot(e) / (dNorm * dNorm * dNorm * 4 * M_PI));
		}
	}
}

double G(const Point3D& x, const Point3D& s)
{
	return 1 / (4 * M_PI * (x - s).Norm());
}

double Potential(const DVector& alphas, const Point3D& xi, const std::vector<Point3D>& s)
{
	double result[4];
	__m256d sum = _mm256_setzero_pd();
	size_t j = 0;
	const double* alphasPtr = alphas.GetConstValues();

	if (DATA_SIZE >= 4)
	{
		for (j; j <= DATA_SIZE - 4; j += 4)
		{
			result[0] = G(xi, s[j]);
			result[1] = G(xi, s[j + 1]);
			result[2] = G(xi, s[j + 2]);
			result[3] = G(xi, s[j + 3]);

			sum = _mm256_add_pd(sum, _mm256_mul_pd(_mm256_load_pd(alphasPtr + j), _mm256_load_pd(result)));
		}
	}

	_mm256_store_pd(result, sum);

	if (DATA_SIZE % 4 != 0)
	{
		for (j; j < DATA_SIZE; j++)
		{
			result[0] += alphas[j] * G(xi, s[j]);
		}
	}

	return result[0] + result[1] + result[2] + result[3];
}

void Potentials(const DVector& alphas, const std::vector<Point3D>& x, const std::vector<Point3D>& s, DVector& potentials)
{
#pragma omp parallel for
	for (int i = 0; i < DATA_SIZE; i++)
	{
		potentials[i] = Potential(alphas, x[i], s);
	}
}

void ExportData(const std::vector<GeoData>& data, const DVector& potentials, const std::string& fileName)
{
	std::ofstream out(fileName);

	if (!out.is_open())
	{
		std::cerr << "Unable to open file!" << std::endl;
	}

	for (size_t i = 0; i < DATA_SIZE; i++)
	{
		out << rad2deg(data[i].B) << " " << rad2deg(data[i].L) << " " << potentials[i] << std::endl;
	}

	out.close();
}

void ParallelMultiply(const DMatrix& A, const DVector& b, DVector& to)
{	
#pragma omp parallel for
	for (int i = 0; i < DATA_SIZE; i++)
	{
		to[i] = A[i].Dot(b);
	}
}

void MulSubMulAddAVX(const DVector& vOld, const DVector& pOld, const DVector& rOld, double Omega, double Beta, DVector& out)
{
	__m256d omega = _mm256_set_pd(Omega, Omega, Omega, Omega);
	__m256d beta = _mm256_set_pd(Beta, Beta, Beta, Beta);
	const double* vVals = vOld.GetConstValues();
	const double* pVals = pOld.GetConstValues();
	const double* rVals = rOld.GetConstValues();
	double* outVals = out.GetValues();

	int j = 0;

	if (DATA_SIZE >= 4)
	{
#pragma omp parallel for lastprivate(j)
		for (j = 0; j <= DATA_SIZE - 4; j += 4)
		{
			_mm256_store_pd(outVals + j, _mm256_add_pd( _mm256_mul_pd( _mm256_sub_pd( _mm256_load_pd(pVals + j) , _mm256_mul_pd( _mm256_load_pd(vVals + j), omega) ), beta), _mm256_load_pd(rVals + j)));
		}
	}

	if (DATA_SIZE % 4 != 0)
	{
		for (j; j < DATA_SIZE; j++)
		{
			outVals[j] = rVals[j] + (pVals[j] - vVals[j] * Omega) * Beta;
		}
	}
}

void MulAdd(const DVector& vec1, const DVector& vec2, double value, DVector& out)
{
	__m256d val = _mm256_set_pd(value, value, value, value);
	const double* vec1Vals = vec1.GetConstValues();
	const double* vec2Vals = vec2.GetConstValues();
	double* outVals = out.GetValues();

	int j = 0;

	if (DATA_SIZE >= 4)
	{
#pragma omp parallel for lastprivate(j)
		for (j = 0; j <= DATA_SIZE - 4; j += 4)
		{
			_mm256_store_pd(outVals + j, _mm256_add_pd(_mm256_mul_pd(_mm256_load_pd(vec1Vals + j), val), _mm256_load_pd(vec2Vals + j)));
		}
	}

	if (DATA_SIZE % 4 != 0)
	{
		for (j; j < DATA_SIZE; j++)
		{
			outVals[j] = vec1Vals[j] * value + vec2Vals[j];
		}
	}
}

void MulSub(const DVector& vec1, const DVector& vec2, double value, DVector& out)
{
	__m256d val = _mm256_set_pd(value, value, value, value);
	const double* vec1Vals = vec1.GetConstValues();
	const double* vec2Vals = vec2.GetConstValues();
	double* outVals = out.GetValues();

	int j = 0;

	if (DATA_SIZE >= 4)
	{
#pragma omp parallel for lastprivate(j)
		for (j = 0; j <= DATA_SIZE - 4; j += 4)
		{
			_mm256_store_pd(outVals + j, _mm256_sub_pd(_mm256_load_pd(vec2Vals + j), _mm256_mul_pd(_mm256_load_pd(vec1Vals + j), val)));
		}
	}

	if (DATA_SIZE % 4 != 0)
	{
		for (j; j < DATA_SIZE; j++)
		{
			outVals[j] = vec2Vals[j] - vec1Vals[j] * value;
		}
	}
}


void BiCGSTAB(const DMatrix& A, const DVector& b, DVector& xNew)
{
	size_t iter = 0;
	DVector rNew(DATA_SIZE);
	DVector rs(DATA_SIZE);
	DVector vNew(DATA_SIZE);
	DVector pNew(DATA_SIZE);
	DVector vOld(DATA_SIZE);
	DVector pOld(DATA_SIZE);
	DVector rOld(DATA_SIZE);
	DVector xOld(DATA_SIZE);
	DVector temp1(DATA_SIZE);
	DVector temp2(DATA_SIZE);
	DVector h(DATA_SIZE);
	DVector s(DATA_SIZE);
	DVector t(DATA_SIZE);

	xNew.SetValues(1.);
	vNew.SetValues(0.);
	pNew.SetValues(0.);

	ParallelMultiply(A, xNew, temp1);

	b.Subtract(temp1, rNew);
	rs.CopyFrom(rNew);
	
	double rhoNew = 1., omegaNew = 1., alpha = 1.;

	while (iter < MAX_ITER)
	{
		iter++;
		double rhoOld = rhoNew;
		double omegaOld = omegaNew;
		vOld.SwapWith(vNew);
		pOld.SwapWith(pNew);
		rOld.SwapWith(rNew);
		xOld.SwapWith(xNew);

		rhoNew = rs.Dot(rOld);
		double beta = (rhoNew / rhoOld) * (alpha / omegaOld);

		MulSubMulAddAVX(vOld, pOld, rOld, omegaOld, beta, pNew);

		ParallelMultiply(A, pNew, vNew);
		alpha = rhoNew / rs.Dot(vNew);

		MulAdd(pNew, xOld, alpha, h);
		MulSub(vNew, rOld, alpha, s);

		if (s.Norm() < TOL)
		{
			xNew.SwapWith(h);
			break;
		}

		ParallelMultiply(A, s, t);
		omegaNew = t.Dot(s) / t.Dot(t);

		MulAdd(s, h, omegaNew, xNew);
		MulSub(t, s, omegaNew, rNew);

		double rNorm = rNew.Norm();
		std::cout << "Reziduum: " << rNorm << std::endl;		

		if (rNorm < TOL || omegaNew == 0.)
		{
			break;
		}		
	}

	std::cout << "Iterations: " << iter << std::endl;
}

void SetB(const std::vector<GeoData> &data, DVector& b)
{
#pragma omp parallel for
	for (int i = 0; i < DATA_SIZE; i++)
	{
		b[i] = data[i].dg * 0.00001;
	}
}

int main(int argc, char** argv)
{
	std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
	std::vector<GeoData> globalData;

	globalData.reserve(DATA_SIZE);
	
	LoadGeoData(fileName, globalData);	

	std::vector<Point3D> x;
	std::vector<Point3D> s;

	x.resize(DATA_SIZE);
	s.resize(DATA_SIZE);
	
	ConvertToXCartesian(globalData, x);
	ConvertToSCartesian(globalData, s);

	DMatrix A(DATA_SIZE, DATA_SIZE);

	FillMatrixParallel(x, s, A);
	
	DVector b(DATA_SIZE);

	SetB(globalData, b);

	DVector alphas(DATA_SIZE);

	BiCGSTAB(A, b, alphas);

	DVector potentials(DATA_SIZE);
	Potentials(alphas, x, s, potentials);
	
	ExportData(globalData, potentials, "Export1.dat");

	
	std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
	auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "Time: " << milliseconds.count() << " milliseconds." << std::endl;

	return 0;
}