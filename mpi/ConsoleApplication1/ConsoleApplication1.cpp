#include <iostream>
#include <mpi.h>
#include <stdio.h>
#include <omp.h>

void InitializeArrayToZero(int* arr, size_t size)
{
	for (size_t i = 0; i < size; i++)
	{
		arr[i] = 0;
	}
}

void InitializeArrayToIncrements(int* arr, size_t size)
{
	for (size_t i = 0; i < size; i++)
	{
		arr[i] = static_cast<int>(i);
	}
}

void main1(int argc, char** argv) {
	int irecv[12] = {};
	int sendcount[4] = { 2, 4, 3, 2 }, displs[4] = { 3, 5, 0, 10 }, recvcounts[4] = { 2, 4, 3, 2 };
	int isend[4];
	// Initialize the MPI environment
	MPI_Init(NULL, NULL);

	// Get the number of processes
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// Get the rank of the process
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

	// Get the name of the processor
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);

	//printf("%d-predMPI: %d %d %d %d\n", world_rank, array[0], array[1], array[2], array[3]);
	//MPI_Bcast(&(array[2]), 2, MPI_INT, 0, MPI_COMM_WORLD);
	//printf("%d-poMPI: %d %d %d %d\n", world_rank, array[0], array[1], array[2], array[3]);

	//isend = world_rank + 1;
	//MPI_Gather(&isend, 1, MPI_INT, irecv, 1, MPI_INT, 0, MPI_COMM_WORLD);
	//MPI_Allgather
	//MPI_Gatherv - variable length msg

	for (int i = 0; i < sendcount[world_rank]; i++)
	{
		isend[i] = world_rank;
	}

	MPI_Gatherv(isend, sendcount[world_rank], MPI_INT, irecv, recvcounts, displs, MPI_INT, 0, MPI_COMM_WORLD);

	if (world_rank == 0)
	{
		for (int i = 0; i < 12; i++)
		{
			printf("%d\n", irecv[i]);
		}
	}

	// Print off a hello world message
	//printf("Hello world from processor %s, rank %d out of %d processors\n",
	//	processor_name, world_rank, world_size);

	// Finalize the MPI environment.
	MPI_Finalize();
}

int main2(int argc, char** argv) {
	// Initialize the MPI environment
	MPI_Init(NULL, NULL);

	// Get the number of processes
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// Get the rank of the process
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

	// Get the name of the processor
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);

	int sum = 0, tmp;
	int istart = world_rank * 3;
	int iend = istart + 2;
	int a[10];

	for (int i = istart; i <= iend; i++)
	{
		a[i] = i;
		sum += a[i];
	}
	printf("Suma na P%d: %d\n", world_rank, sum);
	MPI_Reduce(&sum, &tmp, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	sum = tmp;

	if (!world_rank)
	{
		printf("%d: celkova suma: %d\n", world_rank, sum);
	}

	// Finalize the MPI environment.
	MPI_Finalize();

	return 0;
}

union Test
{
	struct
	{
		float value;
		int position;
	};
	char arr[8];
};

int main3(int argc, char** argv) {
	// Initialize the MPI environment
	MPI_Init(NULL, NULL);

	// Get the number of processes
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// Get the rank of the process
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

	// Get the name of the processor
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);

	int sum = 0;
	int istart = world_rank * 3;
	int iend = istart + 2;
	int a[10] = { 12, 15, 2, 20, 8, 3, 7, 24, 52, 8 };
	float max = -999.f;
	int loc;
	Test isend;
	Test irecv;

	for (int i = istart; i <= iend; i++)
	{
		if (a[i] > max)
		{
			max = static_cast<float>(a[i]);
			loc = i;
		}
	}

	isend.value = max;
	isend.position = loc;

	MPI_Reduce(&isend, &irecv, 1, MPI_FLOAT_INT, MPI_MAXLOC, 0, MPI_COMM_WORLD);

	if (!world_rank)
	{
		printf("%d: celkova max: %f na pozicii %d\n", world_rank, irecv.value, irecv.position);
	}

	// Finalize the MPI environment.
	MPI_Finalize();

	return 0;
}