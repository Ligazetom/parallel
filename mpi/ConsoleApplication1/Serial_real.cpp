#include <iostream>
#include <mpi.h>
#include <stdio.h>
#include <fstream>
#include <cmath>
#include <string>
#include <sstream>
#include <vector>

/********************************/
// Je to otras ze to ma byt jeden .cpp ale tak je to tak

#define M_PI 3.14159265359
#define deg2rad(x) (x * M_PI / 180.)
#define rad2deg(x) (x * 180. / M_PI)

constexpr int EARTH_RADIUS_KM = 6378000;	/*6378*/
constexpr double GM = 398600.5;
constexpr int D = 500;
constexpr int MAX_ITER = 100;
constexpr double TOL = 1e-9;

class DVector
{
public:
	DVector();
	DVector(size_t length);

	DVector(const DVector &vec);
	DVector(DVector &&vec);
	DVector &operator=(const DVector &vec);
	DVector &operator=(DVector &&vec);
	~DVector();

	void SetLength(size_t length);
	size_t GetLength() const;
	double *GetValues();
	void SetValues(double val);
	const double* GetConstValues() const;
	void AddElement(double val);

	DVector operator*(double val) const;
	DVector operator/(double val) const;
	DVector operator-(const DVector &vec) const;
	DVector operator+(const DVector &vec) const;
	double& operator[](size_t i);
	double operator[](size_t i) const;

	double Dot(const DVector& vec) const;
	double Norm() const;
	void Normalize();
	DVector Normalized() const;

private:
	double *m_values;
	size_t m_length;

	void AllocateDataArray(size_t length);
	void DeallocateDataArray();
};

DVector::DVector()
{
	m_values = nullptr;
	m_length = 0;
}

DVector::DVector(size_t length)
{
	m_values = nullptr;
	this->m_length = length;

	AllocateDataArray(length);

	for (size_t i = 0; i < length; i++)
	{
		m_values[i] = 0.;
	}
}

DVector::DVector(const DVector &vec)
{
	AllocateDataArray(vec.m_length);
	m_length = vec.m_length;

	for (size_t i = 0; i < m_length; i++)
	{
		m_values[i] = vec.m_values[i];
	}
}

DVector::DVector(DVector &&vec)
{
	m_values = vec.m_values;
	vec.m_values = nullptr;
	m_length = vec.m_length;
}

DVector &DVector::operator=(const DVector &vec)
{
	DeallocateDataArray();

	if (m_values == nullptr) {
		AllocateDataArray(vec.m_length);

		for (size_t i = 0; i < vec.m_length; i++)
		{
			m_values[i] = vec.m_values[i];
		}
		m_length = vec.m_length;
	}

	return *this;
}

DVector &DVector::operator=(DVector &&vec)
{
	DeallocateDataArray();

	if (m_values == nullptr) {
		m_values = vec.m_values;
		vec.m_values = nullptr;
		m_length = vec.m_length;
	}

	return *this;
}

double& DVector::operator[](size_t i)
{
	return m_values[i];
}

double DVector::operator[](size_t i) const
{
	return m_values[i];
}

DVector::~DVector()
{
	DeallocateDataArray();
}

void DVector::SetValues(double val)
{
	for (size_t i = 0; i < m_length; i++)
	{
		m_values[i] = val;
	}
}

void DVector::AllocateDataArray(size_t length)
{
	m_values = new double[length];
}

void DVector::DeallocateDataArray()
{
	if (m_values != nullptr) {
		delete[] m_values;
	}

	m_values = nullptr;
}

size_t DVector::GetLength() const
{
	return m_length;
}

double *DVector::GetValues()
{
	return m_values;
}

void DVector::SetLength(size_t length)
{
	DeallocateDataArray();
	AllocateDataArray(length);
	this->m_length = length;
}

DVector DVector::operator*(double val) const
{
	DVector buff(m_length);

	for (size_t i = 0; i < m_length; i++)
	{
		buff.m_values[i] = m_values[i] * val;
	}

	return buff;
}

DVector DVector::operator/(double val) const
{
	DVector buff(m_length);

	for (size_t i = 0; i < m_length; i++)
	{
		buff.m_values[i] = m_values[i] / val;
	}

	return buff;
}

DVector DVector::operator-(const DVector &vec) const
{
	DVector buff(m_length);

	for (size_t i = 0; i < m_length; i++)
	{
		buff.m_values[i] = m_values[i] - vec.m_values[i];
	}

	return buff;
}

DVector DVector::operator+(const DVector &vec) const
{
	DVector buff(m_length);

	for (size_t i = 0; i < m_length; i++)
	{
		buff.m_values[i] = m_values[i] + vec.m_values[i];
	}

	return buff;
}

double DVector::Norm() const
{
	double buff = 0;

	for (size_t i = 0; i < m_length; i++)
	{
		buff += (m_values[i] * m_values[i]);
	}

	return sqrt(buff);
}

void DVector::AddElement(double val)
{
	double *newData = new double[m_length + 1];

	for (size_t i = 0; i < m_length; i++)
	{
		newData[i] = m_values[i];
	}

	newData[m_length] = val;

	DeallocateDataArray();

	m_values = newData;
	m_length++;
}

const double* DVector::GetConstValues() const
{
	return m_values;
}

double DVector::Dot(const DVector& vec) const
{
	double buff = 0;

	for (size_t i = 0; i < m_length; i++)
	{
		buff += m_values[i] * vec.m_values[i];
	}

	return buff;
}

void DVector::Normalize()
{
	double norm = Norm();

	for (size_t i = 0; i < m_length; i++)
	{
		m_values[i] /= norm;
	}
}

DVector DVector::Normalized() const
{
	DVector temp(*this);

	temp.Normalize();

	return temp;
}

class DMatrix
{
public:
	DMatrix(size_t columns, size_t rows);
	DMatrix(const DMatrix &mat);
	DMatrix(DMatrix &&mat);
	DMatrix& operator=(const DMatrix &mat);
	DMatrix& operator=(DMatrix &&mat);
	DVector& operator[](size_t i);
	DVector operator[](size_t i) const;
	~DMatrix();

	void InsertBlock(const DMatrix& mat, int row, int col);
	size_t ColumnCount() const;
	size_t RowCount() const;
	DMatrix operator*(const DMatrix &mat) const;
	DVector operator*(const DVector &vec) const;
	DMatrix operator*(double val) const;
	DVector GetRow(const int index) const;
	DVector *GetRows();
	const DVector* GetConstColumns() const;
	DMatrix Transpose() const;
	DVector ColumnToVector(int colIndex) const;
	void MakeIdentity();
	void SwapColumns(int index1, int index2);
	void SwapRows(int index1, int index2);
	void SetRow(int index, const DVector& vec);
	void SetColumn(int index, const DVector& vec);

private:
	DVector *m_rows;
	size_t m_columnCount;
	size_t m_rowCount;

	void DeallocateDataArray();
	void AllocateDataArray(size_t count);
	DMatrix CopyThisObject() const;
};

DMatrix::DMatrix(size_t columns, size_t rows)
{
	m_rows = nullptr;
	AllocateDataArray(rows);

	for (size_t i = 0; i < rows; i++)
	{
		m_rows[i].SetLength(columns);
	}

	this->m_columnCount = columns;
	this->m_rowCount = rows;
}

DMatrix::DMatrix(const DMatrix &mat)
{
	m_rows = nullptr;
	AllocateDataArray(mat.m_rowCount);

	for (int i = 0; i < mat.m_rowCount; i++)
	{
		m_rows[i].SetLength(mat.m_columnCount);
		m_rows[i] = mat.m_rows[i];
	}

	m_columnCount = mat.m_columnCount;
	m_rowCount = mat.m_rowCount;
}

DMatrix::DMatrix(DMatrix &&mat)
{
	m_rows = mat.m_rows;
	mat.m_rows = nullptr;

	m_columnCount = mat.m_columnCount;
	m_rowCount = mat.m_rowCount;
}

DMatrix &DMatrix::operator=(const DMatrix &mat)
{
	DeallocateDataArray();

	if (m_rows == nullptr)
	{
		AllocateDataArray(mat.m_rowCount);

		for (int i = 0; i < mat.m_rowCount; i++)
		{
			m_rows[i].SetLength(mat.m_columnCount);
			m_rows[i] = mat.m_rows[i];
		}
	}

	m_columnCount = mat.m_columnCount;
	m_rowCount = mat.m_rowCount;

	return *this;
}

DMatrix &DMatrix::operator=(DMatrix &&mat)
{
	DeallocateDataArray();

	m_rows = mat.m_rows;
	mat.m_rows = nullptr;

	m_columnCount = mat.m_columnCount;
	m_rowCount = mat.m_rowCount;

	return *this;
}

DVector& DMatrix::operator[](size_t i)
{
	return m_rows[i];
}

DVector DMatrix::operator[](size_t i) const
{
	return m_rows[i];
}

DMatrix::~DMatrix()
{
	DeallocateDataArray();
}

void DMatrix::InsertBlock(const DMatrix& mat, int row, int col)
{
	for (int i = row; i < row + mat.m_rowCount; i++)
	{
		for (int j = col; j < col + mat.m_columnCount; j++)
		{
			m_rows[i].GetValues()[j] = mat.m_rows[i - row].GetValues()[j - col];
		}
	}
}

void DMatrix::DeallocateDataArray()
{
	if (m_rows != nullptr) {
		delete[] m_rows;
	}

	m_rows = nullptr;
}

void DMatrix::AllocateDataArray(size_t count)
{
	m_rows = new DVector[count];
}

DMatrix DMatrix::operator*(const DMatrix &mat) const
{
	DMatrix mBuff(mat.m_columnCount, mat.m_rowCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		for (int j = 0; j < mat.m_columnCount; j++)
		{
			DVector matCol(mat.ColumnToVector(j));
			mBuff.m_rows[i].GetValues()[j] = m_rows[i].Dot(matCol);
		}
	}

	return mBuff;
}

DVector DMatrix::operator*(const DVector &vec) const
{
	DVector vBuff(m_rowCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		vBuff.GetValues()[i] = m_rows[i].Dot(vec);
	}

	return vBuff;
}

DMatrix DMatrix::operator*(double val) const
{
	DMatrix buff(m_columnCount, m_rowCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		buff.m_rows[i] = m_rows[i] * val;
	}

	return buff;
}

DVector DMatrix::GetRow(int index) const
{
	return m_rows[index];
}

DVector DMatrix::ColumnToVector(int colIndex) const
{
	DVector buff(m_rowCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		buff.GetValues()[i] = m_rows[i].GetValues()[colIndex];
	}

	return buff;
}

DVector *DMatrix::GetRows()
{
	return m_rows;
}

DMatrix DMatrix::Transpose() const
{
	DMatrix mBuff(m_rowCount, m_columnCount);

	for (int i = 0; i < m_rowCount; i++)
	{
		mBuff.m_rows[i] = ColumnToVector(i);
	}

	return mBuff;
}

size_t DMatrix::ColumnCount() const
{
	return m_columnCount;
}

size_t DMatrix::RowCount() const
{
	return m_rowCount;
}

void DMatrix::MakeIdentity() {
	for (int i = 0; i < m_rowCount; i++)
	{
		for (int j = 0; j < m_columnCount; j++)
		{
			if (i == j)
			{
				m_rows[i].GetValues()[j] = 1.;
			}
			else
			{
				m_rows[i].GetValues()[j] = 0.;
			}
		}
	}
}

void DMatrix::SwapRows(int index1, int index2)
{
	DVector buff;

	buff = m_rows[index1];
	m_rows[index1] = m_rows[index2];
	m_rows[index2] = buff;
}

void DMatrix::SwapColumns(int index1, int index2)
{
	DVector buff1, buff2;

	buff1 = ColumnToVector(index1);
	buff2 = ColumnToVector(index2);

	SetColumn(index1, buff1);
	SetColumn(index2, buff2);
}

void DMatrix::SetRow(int index, const DVector& vec)
{
	m_rows[index] = vec;
}

void DMatrix::SetColumn(int index, const DVector& vec)
{
	for (int i = 0; i < m_rowCount; i++)
	{
		m_rows[i].GetValues()[index] = vec.GetConstValues()[i];
	}
}

DMatrix DMatrix::CopyThisObject() const
{
	DMatrix newMat(m_columnCount, m_rowCount);

	for (int i = 0; i < m_columnCount; i++)
	{
		for (int j = 0; j < m_rowCount; j++)
		{
			newMat.GetRows()[i].GetValues()[j] = m_rows[i].GetValues()[j];
		}
	}

	return newMat;
}

const DVector* DMatrix::GetConstColumns() const
{
	return m_rows;
}

struct GeoData
{
	double B;
	double L;
	double H;
	double dg;
	double Tzz;
};

void LoadGeoData(const std::string& pathToFile, std::vector<GeoData>& dataVec)
{
	std::ifstream inFile(pathToFile);

	if (!inFile.is_open())
	{
		std::cerr << "Could not open file!";
	}

	double first, second, third, fourth, fifth;

	while (inFile >> first >> second >> third >> fourth >> fifth)
	{
		GeoData temp;

		temp.B = deg2rad(first);
		temp.L = deg2rad(second);
		temp.H = third;
		temp.dg = fourth;
		temp.Tzz = fifth;

		dataVec.push_back(temp);
	}

	inFile.close();
}

void ConvertToXCartesian(const std::vector<GeoData>& from, std::vector<DVector>& to)
{
	to.reserve(from.size());

	for (size_t i = 0; i < from.size(); i++)
	{
		DVector temp(3);
		
		temp.GetValues()[0] = (EARTH_RADIUS_KM + from[i].H) * cos(from[i].B) * cos(from[i].L);
		temp.GetValues()[1] = (EARTH_RADIUS_KM + from[i].H) * cos(from[i].B) * sin(from[i].L);
		temp.GetValues()[2] = (EARTH_RADIUS_KM + from[i].H) * sin(from[i].B);

		/*temp.GetValues()[0] = EARTH_RADIUS_KM * cos(from[i].B) * cos(from[i].L);
		temp.GetValues()[1] = EARTH_RADIUS_KM * cos(from[i].B) * sin(from[i].L);
		temp.GetValues()[2] = EARTH_RADIUS_KM * sin(from[i].B);*/

		to.push_back(temp);
	}
}

void ConvertToSCartesian(const std::vector<GeoData>& from, std::vector<DVector>& to)
{
	to.reserve(from.size());

	for (size_t i = 0; i < from.size(); i++)
	{
		DVector temp(3);

		temp.GetValues()[0] = (EARTH_RADIUS_KM + from[i].H - D) * cos(from[i].B) * cos(from[i].L);
		temp.GetValues()[1] = (EARTH_RADIUS_KM + from[i].H - D) * cos(from[i].B) * sin(from[i].L);
		temp.GetValues()[2] = (EARTH_RADIUS_KM + from[i].H - D) * sin(from[i].B);

	/*	temp.GetValues()[0] = (EARTH_RADIUS_KM - D) * cos(from[i].B) * cos(from[i].L);
		temp.GetValues()[1] = (EARTH_RADIUS_KM - D) * cos(from[i].B) * sin(from[i].L);
		temp.GetValues()[2] = (EARTH_RADIUS_KM - D) * sin(from[i].B);*/

		to.push_back(temp);
	}
}

void FillMatrix(const std::vector<DVector>& x, const std::vector<DVector>& s, DMatrix& A)
{
	for (size_t i = 0; i < x.size(); i++)
	{
		for (size_t j = 0; j < s.size(); j++)
		{
			DVector d = x[i] - s[j];
			DVector e = x[i].Normalized();
			double dNorm = d.Norm();

			A[i][j] = (d.Dot(e) / (dNorm * dNorm * dNorm * 4 * M_PI));
		}
	}
}

DVector BiCGSTAB(const DMatrix& A, const DVector& b)
{
	size_t iter = 0;
	DVector xNew(A.RowCount());
	xNew.SetValues(1.);
	DVector rNew = b - A * xNew;
	DVector rs = rNew;
	DVector vNew(xNew.GetLength()), pNew(xNew.GetLength());
	vNew.SetValues(0.);
	pNew.SetValues(0.);
	double rhoNew = 1., omegaNew = 1., alpha = 1.;
	
	while (iter < MAX_ITER)
	{
		iter++;
		std::cout << "Iter: " << iter << std::endl;
		double rhoOld = rhoNew;
		double omegaOld = omegaNew;
		DVector vOld = vNew;
		DVector pOld = pNew;
		DVector rOld = rNew;
		DVector xOld = xNew;

		rhoNew = rs.Dot(rOld);
		double beta = (rhoNew / rhoOld) * (alpha / omegaOld);
		pNew = rOld + (pOld - vOld * omegaOld) * beta;
		vNew = A * pNew;
		alpha = rhoNew / rs.Dot(vNew);
		DVector h = xOld + pNew * alpha;
		DVector s(rOld - vNew * alpha);

		if (s.Norm() < TOL)
		{
			xNew = h;
			return xNew;
		}

		DVector t = A * s;
		omegaNew = t.Dot(s) / t.Dot(t);
		xNew = h + s * omegaNew;

		if ((xOld - xNew).Norm() < TOL)
		{
			return xNew;
		}

		rNew = s - t * omegaNew;
	}

	return xNew;
}

double G(const DVector& x, const DVector& s)
{
	return 1 / (4 * M_PI * (x - s).Norm());
}

double Potential(const DVector& alphas, const DVector& xi, const std::vector<DVector>& s)
{
	double accum = 0.;

	for (size_t j = 0; j < s.size(); j++)
	{
		accum += alphas[j] * G(xi, s[j]);
	}

	return accum;
}

DVector Potentials(const DVector& alphas, const std::vector<DVector>& x, const std::vector<DVector>& s)
{
	DVector temp(x.size());

	for (size_t i = 0; i < x.size(); i++)
	{
		temp[i] = Potential(alphas, x[i], s);
	}

	return temp;
}

void ExportData(const std::vector<GeoData>& data, const DVector& potentials, const std::string& fileName)
{
	std::ofstream out(fileName);

	if (!out.is_open())
	{
		std::cerr << "Unable to open file!" << std::endl;
	}

	for (size_t i = 0; i < potentials.GetLength(); i++)
	{
		out << rad2deg(data[i].B) << " " << rad2deg(data[i].L) << " " << potentials[i] << std::endl;
	}

	out.close();
}

int main(int argc, char** argv)
{
	std::string pathToData("./BL-902.dat");
	size_t numOfRecords = 902;

	MPI_Init(NULL, NULL);

	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	int prank;
	MPI_Comm_rank(MPI_COMM_WORLD, &prank);

	//*****************************//
	//
	//
	//		Zaujimave veci
	//
	//****************************//

	std::vector<GeoData> data;
	data.reserve(numOfRecords);

	LoadGeoData(pathToData, data);
	std::vector<DVector> x;
	std::vector<DVector> s;

	ConvertToXCartesian(data, x);
	ConvertToSCartesian(data, s);
	DMatrix A(s.size(), x.size());
	FillMatrix(x, s, A);

	DVector b(A.RowCount());
	/*b.SetValues(GM / (EARTH_RADIUS_KM * EARTH_RADIUS_KM));*/


	for (size_t i = 0; i < b.GetLength(); i++)
	{
		b[i] = data[i].dg * 0.00001;
	}

	DVector alphas(BiCGSTAB(A, b));

	std::cout << "Potents\n";

	DVector potentials(Potentials(alphas, x, s));

	ExportData(data, potentials, "Export1.dat");

	return 0;
}